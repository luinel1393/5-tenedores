## 5 Tenedores

Es una App que sirve para mostrar una lista de restaurantes y datos acerca de los mismos donde los usuarios pueden evaluar y dar opiniones sobre ellos e incluso, pueden agregar nuevos restaurantes. Cuenta con un sistema de login completo.

Fue desarrollada siguiendo el curso de React Native de Agustín Navarro Galdon disponible en la plataforma de Udemy en:
[React Native Expo: Creando un TripAdvisor de Restaurantes](https://www.udemy.com/course/react-native-expo-creando-mini-tripadvisor-de-restaurantes/)

### Tecnologías

Algunas de las tecnologías utilizadas son:

- React Native y Hooks
- [Expo](https://expo.io)
- Node
- Firebase
- Yarn para el manejo de dependencias

### Instalación e iniciación del servicio en ambiente de desarrollo

Instalar las depedencias e iniciar el servicio de la app.

```sh
$ cd 5-tenedores
$ yarn
$ yarn dev
```

Una vez iniciado el servicio se verá por consola y por una pestaña del navegador información de la aplicación que se está ejecutando, destacando un código QR.

### Iniciar app en ambiente de desarrollo

Si se quiere ejecutar desde un emulador de Android o iOS instalado en el ordenador, se debe iniciar el respectivo emulador y bastaría con pulsar la letra que identifica el sistema operativo según sea el caso.

```sh
$ a
```

ó

```sh
$ i
```

Eso instalará Expo en el emulador y ejecutará la app.

En caso de que se quiere ejecutar la aplicación en un dispostivo físico, se debe tener instalada la aplicación de Expo para [Android](https://play.google.com/store/apps/details?id=host.exp.exponent&hl=es_AR) o [iOS](https://apps.apple.com/us/app/expo-client/id982107779) según sea el caso y luego escanear el código QR.
