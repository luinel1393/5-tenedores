import firebase from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyDu_9EsI1o7QKA57sHJ-r8BuLWmxaNqvUk",
    authDomain: "tenedores-c1443.firebaseapp.com",
    databaseURL: "https://tenedores-c1443.firebaseio.com",
    projectId: "tenedores-c1443",
    storageBucket: "tenedores-c1443.appspot.com",
    messagingSenderId: "533341883751",
    appId: "1:533341883751:web:030a69205651c8fe4ab3c2"
}

export const firebaseApp = firebase.initializeApp(firebaseConfig);